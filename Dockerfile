FROM python:3.7.4

WORKDIR /app
ADD main.py /app
ADD requirements.txt /app

RUN pip install -r requirements.txt

ENTRYPOINT [ "python", "main.py" ]
