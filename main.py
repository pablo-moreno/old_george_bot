#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""
import os
import logging
import time

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters,
                          ConversationHandler)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

(
    START, HELLO, STATUS, IS_YOUR_BIRTHDAY, I_KNOW_WHO_YOU_ARE,
    WANNA_ANSWERS, I_WILL_GIVE_YOU_ANSWERS, CONQUER_THE_WORLD,
    EXPLAIN_THE_WAR, ACT, ACT_NOW, ACT_LATER,
    GO_FUENMAYOR, ACCEPT_MISSION, GO_MISSION, THE_END,
) = range(16)


def start(update, context):
    reply_keyboard = [['Bien', 'Mal', 'Regular']]

    update.message.reply_text(
        'Hola muchacho! ¿qué tal va todo?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard))

    return HELLO


def wrapper(reply_keyboard, reply_text_fn, stage):
    def fn(update):
        reply_text = reply_text_fn(update)
        update.message.reply_text(reply_text,
                                  reply_markup=ReplyKeyboardMarkup([reply_keyboard]))

        return stage

    return fn


def how_are_you(update):
    if update.message.text == 'Bien':
        return 'Me alegro, chaval'
    else:
        return 'Vaya hombre, todos tenemos días malos'


def status(update, context):
    user = update.message.from_user
    reply_keyboard = [['Si', 'No']]

    if update.message.text == 'Bien':
        update.message.reply_text('Me alegro chaval. Yo también, gracias por preguntar.')
    else:
        update.message.reply_text('Vaya, hombre, seguro que no es para tanto.')

    update.message.reply_text('He oído que es tu cumpleaños, ¿es cierto?',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard))

    return STATUS


def is_your_birthday_answer(update, context):
    user = update.message.from_user
    reply_keyboard = [['???', 'wtf?']]

    if update.message.text == 'Si':
        reply_text = 'Lo sabía. Porque esto ya lo he vivido. Hace 60 años.'
    else:
        reply_text = 'No mientas. Ambos sabemos que sí. Llevo 60 años esperando este momento.'

    update.message.reply_text(reply_text,
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard))

    return IS_YOUR_BIRTHDAY


def i_know_who_you_are(update, context):
    reply_keyboard = [['Que te den!', '¿Pero qué cojones?']]
    update.message.reply_text('Sé quién eres, porque yo mismo he pasado por esto.',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard))

    return I_KNOW_WHO_YOU_ARE


def do_you_want_answers(update, context):
    reply_keyboard = [['Si', 'Tengo otras cosas más importantes que hacer']]
    update.message.reply_text('¿Quieres una respuesta a este misterio?',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard))

    return I_WILL_GIVE_YOU_ANSWERS


def i_will_give_you_answers(update, context):
    reply_keyboard = [['Tengo otras cosas más importantes que hacer', ]]
    update.message.reply_text('Da igual lo que tu quieras. Tu vida corre peligro')
    update.message.reply_text('Ahora es hora de que bailes para mi')
    update.message.reply_text('MUAAAAHAHAHAAHAHA',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard))

    return CONQUER_THE_WORLD


def conquer_the_world(update, context):
    reply_keyboard = [[
        'Muy interesante, lo anotaré en mi máquina de escribir invisible',
        'Bien, por fin tendremos gobierno',
    ]]
    update.message.reply_text('Dominar a todos los humanos... PERDÓN, FALLO DEL SISTEMA',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard))

    return EXPLAIN_THE_WAR


def explain_the_war(update, context):
    reply_keyboard = [[
        'Bah, paso',
        '¿Yo qué puedo hacer? solo soy un hombre',
    ]]
    update.message.reply_text('Como te iba diciendo, vengo del futuro y la humanidad ha sido destruida '
                              'por una guerra mundial entre youtubers y terraplanistas.'
                              'Solo tú puedes evitarlo',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard))

    return ACT


def act(update, context):
    reply_keyboard = [[
        '¿Yo qué puedo hacer? solo soy un hombre',
        'Pf, bueno, venga, vale, ok',
    ]]

    if update.message.text == 'Bah, paso':
        update.message.reply_text('Tu deber como ciudadano de bien es el de detener esta catástrofe mundial, parguela!',
                                  reply_markup=ReplyKeyboardMarkup(reply_keyboard))
        return ACT
    else:
        return GO_FUENMAYOR


def go_fuenmayor(update, context):
    reply_keyboard = [[
        '¿Fuenmayor? ¿eso existe?',
        'Si tu lo dices...',
    ]]

    update.message.reply_text('Debes partir de inmediato a la Tierra Perdida '
                              '(O sea, que vayas a Fuenmayor) y seguir el camino del fuego.'
                              'Solo así obtendras las respuestas que buscas.',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard))
    return ACCEPT_MISSION


def accept_mission(update, context):
    reply_keyboard = [[
        'Mejor lo busco en Google Maps',
        'Entiendo...'
    ]]
    if 'Fuenmayor' in update.message.text:
        update.message.reply_text('La segunda estrella a la derecha. Entre Albacete y Alcobendas',
                                  reply_markup=ReplyKeyboardMarkup(reply_keyboard))
        return ACCEPT_MISSION
    else:
        return GO_MISSION


def go_on_your_mission(update, context):
    reply_keyboard = [[
        'No me llamo Jorge Javier, gilipollas',
        'Me he cortado el pelo y ahora soy un punky abertzale. Creo que estoy preparado.',
    ]]
    update.message.reply_text('Ahora debes partir, Jorge Javier. ')
    update.message.reply_text('Y recuerda... el destino de la humanidad depende de ti... ',
                              reply_markup=ReplyKeyboardMarkup(reply_keyboard))

    return THE_END


def the_end(update, context):
    update.message.reply_text('de ti...', reply_markup=ReplyKeyboardRemove())
    time.sleep(1)
    update.message.reply_text('de ti...')
    time.sleep(1)
    update.message.reply_text('de ti...')
    time.sleep(1)
    update.message.reply_text('uuuuuuhhhhhhhhh...')
    time.sleep(1)
    update.message.reply_text('** neblina desvanecedora **')

    return ConversationHandler.END


def cancel(update, context):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                              reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    token = os.environ.get('TELEGRAM_BOT_TOKEN')
    updater = Updater(token, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            START: [MessageHandler(Filters.text, start)],
            HELLO: [MessageHandler(Filters.text, status)],
            STATUS: [MessageHandler(Filters.text, is_your_birthday_answer)],
            IS_YOUR_BIRTHDAY: [MessageHandler(Filters.text, i_know_who_you_are)],
            I_KNOW_WHO_YOU_ARE: [MessageHandler(Filters.text, do_you_want_answers)],
            I_WILL_GIVE_YOU_ANSWERS: [MessageHandler(Filters.text, i_will_give_you_answers)],
            CONQUER_THE_WORLD: [MessageHandler(Filters.text, conquer_the_world)],
            EXPLAIN_THE_WAR: [MessageHandler(Filters.text, explain_the_war)],
            ACT: [MessageHandler(Filters.text, act)],
            GO_FUENMAYOR: [MessageHandler(Filters.text, go_fuenmayor)],
            ACCEPT_MISSION: [MessageHandler(Filters.text, accept_mission)],
            GO_MISSION: [MessageHandler(Filters.text, go_on_your_mission)],
            THE_END: [MessageHandler(Filters.text, the_end)],
        },
        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
